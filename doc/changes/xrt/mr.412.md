add `xrt_device_type` to `xrt_device` to differentiate handed controllers
from controllers that can be held in either hand.
