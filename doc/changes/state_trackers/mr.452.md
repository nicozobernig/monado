OpenXR: Properly handle more than one input source being bound to the same action
according to the combination rules of the specification.
