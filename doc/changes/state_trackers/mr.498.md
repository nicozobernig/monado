OpenXR: Add support for creating swapchains with depth formats and submitting depth layers. The depth layers are passed through to the compositor, but are not used yet.
