OpenXR: Minor fixes for various bits of code: copy-typo in device assignment
code; better stub for the unimplemented function
`xrEnumerateBoundSourcesForAction`; better error message on internal error in
`xrGetCurrentInteractionProfile`.
