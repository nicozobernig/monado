st/oxr: Fix crash when calling `xrPollEvents` when headless mode is selected.
