compositor: Fix printing of current connected displays on nvidia when no whitelisted display is found.
