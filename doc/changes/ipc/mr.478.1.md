client: Implement the usage of the `xrt_image_native_allocator`, currently not
used. But it is needed for platforms where for various reasons the allocation
must happen on the client side.
